import os
import re
path = "F:/MDA/2018txt" #存放txt文件夹目录
files= os.listdir(path) #得到文件夹下的所有文件名称
print(len(files))

for file in files: #遍历文件夹
    txts = []
    position = path+'/'+ file #构造绝对路径，"\\"，其中一个'\'为转义符
    print (position)           
    with open(position, "r",encoding='utf-8') as f:    #打开文件
        data = f.read()   #读取文件
        txts.append(data)
    txts = ','.join(txts)#转化为非数组类型  
    
    txts = txts.replace("\n","")
    txts = txts.replace("\r","")
    txts=txts.replace(" ","")
    txts=txts.replace('管理层讨论与分析',"",1)
    pattern=re.compile('管理层讨论与分析(.+)第四节') #匹配ab与ef之间的内容
    result=pattern.findall(txts)
    result = ','.join(result)#转化为非数组类型  
    file1 = "F:/MDA/2018retxt/" + file[:9] + '.txt'
    with open(file1, 'w+', encoding='utf8') as f:
        f.write(result)
