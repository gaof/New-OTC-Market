import os
import re
import jieba
import numpy as np
import pandas as pd
path = "F:/MDA/2020retxt" #文件夹目录
files= os.listdir(path) #得到文件夹下的所有文件名称
pos_neg_2018 = pd.DataFrame(columns=['ID','pos1','neg1','pos2'])  

#pos_neg_2018.columns = ['ID','pos1','pos2','neg1','neg2']
stopwords = [line.rstrip() for line in open("F:/基于多源数据融合的中小企业信用风险研究/shuju/MDA/stopwords.txt", 'r', encoding='utf-8')]
neg = [line.rstrip() for line in open("F:/基于多源数据融合的中小企业信用风险研究/shuju/MDA/中文金融词典/中文金融词典/dict/formal_neg.txt", 'r', encoding='utf-8')]
pos = [line.rstrip() for line in open("F:/基于多源数据融合的中小企业信用风险研究/shuju/MDA/中文金融词典/中文金融词典/dict/formal_pos.txt", 'r', encoding='utf-8')]
for file in files: #遍历文件夹
    txts = []
    position = path+'/'+ file 
    print (position)           
    with open(position, "r",encoding='utf-8') as f:    #打开文件
        data = f.read()   #读取文件
        txts.append(data)
    txts = ','.join(txts)#转化为非数组类型
    txts = re.sub(r'\d+', '', txts) #去除数字
    txts = re.sub(r'\n', '', txts)
    seg_list = jieba.cut(txts)
    
    cut_txts = list(seg_list)
    outstr = []
    
    for word in cut_txts:
        if word not in stopwords:  # 去停用词 + 词性筛选           
            outstr.append(word)
    lineswords=list(outstr)
    
    list_set=[]
    for i in lineswords:
        #print(i)
        list_set.append(i)
    from collections import Counter

    words_count = Counter(list_set)
    statics = words_count.most_common()

    
    posnum=0;
    negnum=0;
    num=0;
   
    for static in statics:
        num+=static[1]
        if static[0] in pos:     
            posnum+=static[1];
        if static[0] in neg:     
            negnum+=static[1];

    pos1 = (posnum/num)*100

    neg1 = (negnum/num)*100
    
    pos2 = ((pos1-neg1)/(pos1+neg1))*100

    #add row to end of DataFrame
    pos_neg_2018.loc[len(pos_neg_2018.index)] = [file[:9], pos1, neg1,pos2]

# print(pos_neg_2020)
